unit c_parser;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, strutils, c_interfaces, c_loaders;

const
  // подлежащие парсингу тэги
  PTags: array [1..2] of string = ('input', 'textarea');
  // тэги, которые удаляются при парсинге
  PExcludeTags: array [1..3] of string = ('form', '/form', '/textarea');
  // разделители, которые позволяют извлечь только имя тэга из строки
  WDelim = [' ', '<', '>'];
  JC_EMPTYBOX = '&#9744;&nbsp;';            // пустой бокс для HTML
  JC_CHECKEDBOX = '&#9745;&nbsp;';          // отмеченный бокс для HTML
  CRLF = #13#10;                            // перенос строки заменяется на <br>

type

  { TMyHTMLParser }

  TMyHTMLParser = class(TCustomHTMLParser)
  protected
    fReady: boolean;
    HtmlFile: TStringList;
    Loader: IHtmlLoader;
    Saver: IHtmlSaver;
    Doc: IAbstractDoc;
    procedure AddTagToMapIfExist(onestr: string);
    function ParseOneString(onestr: string): string;
  public
    function Ready: boolean; override;
    function HtmlAsWString: WideString; override;
    procedure DoParse(_target: string); override;
    procedure AssignDoc(ADoc: IAbstractDoc); override;
    constructor Create(Source: string; ls: cLoadSource); override;
    destructor Destroy; override;
  end;

function GetTagName(Tag: string): string;
function GetTagAttribute(Tag, Attribute: string): string;
function InTags(Tag: string; const TagSet: array of string): boolean;

implementation

{ TMyHTMLParser }

function TMyHTMLParser.Ready: boolean;
begin
  exit(fReady);
end;

function TMyHTMLParser.HtmlAsWString: WideString;
begin
  if HtmlFile <> nil then
    Result := WideString(HtmlFile.Text)
  else
    Result := '';
end;

procedure TMyHTMLParser.DoParse(_target: string);
var
  i: integer;
begin
  if (not Ready) or (HtmlFile.Count <= 0) then
    exit;
  for i := 0 to HtmlFile.Count - 1 do
  begin
    HtmlFile[i] := ParseOneString(HtmlFile[i]);
    HTMLFile[i] := StringReplace(HTMLFile[i], CRLF, '<br>', [rfReplaceAll]);
  end;
  Saver := HtmlSaveToFile.Create;
  Saver.Save(_target, HtmlFile);
end;

procedure TMyHTMLParser.AssignDoc(ADoc: IAbstractDoc);
var
  Cur: string;
begin
  if ADoc <> nil then
  begin
    Doc := ADoc;
    if HtmlFile.Count > 0 then
    begin
      for Cur in HtmlFile do
        AddTagToMapIfExist(Cur);
      fReady := True;
    end;
  end;
end;

procedure TMyHTMLParser.AddTagToMapIfExist(onestr: string);
var
  CurTag, FWord, FName, FType: string;
begin
  CurTag := GetTagName(onestr);
  FWord := ExtractWord(1, CurTag, WDelim);
  if not InTags(FWord, PTags) then
    exit;
  FName := GetTagAttribute(CurTag, 'name');
  FName := MidStr(Fname, 7, Length(FName) - 7);
  FType := GetTagAttribute(CurTag, 'type');
  if MidStr(FType, 7, Length(FType) - 7) = 'checkbox' then
    FType := 'bool'
  else
    FType := 'str';
  Doc.Add(FName, Ftype, '');
end;

function TMyHTMLParser.ParseOneString(onestr: string): string;
var
  CurTag, FWord, FName, KeyType, Value: string;
begin
  CurTag := GetTagName(onestr);
  FWord := ExtractWord(1, CurTag, WDelim);
  if InTags(FWord, PExcludeTags) then
    exit(ReplaceStr(onestr, CurTag, 'p'));
  if not InTags(FWord, PTags) then
    exit(onestr);
  FName := GetTagAttribute(CurTag, 'name');
  FName := MidStr(Fname, 7, Length(FName) - 7);
  if Doc.TryToGet(FName, KeyType, Value) then
  begin
    if KeyType = 'bool' then
      case Value of
        '': Value := JC_EMPTYBOX;
        'On': Value := JC_CHECKEDBOX;
      end;
    Result := StringReplace(onestr, '<' + CurTag + '>', Value, [rfReplaceAll]);
  end
  else
    Result := onestr;
end;

constructor TMyHTMLParser.Create(Source: string; ls: cLoadSource);
begin
  HtmlFile := TStringList.Create;
  fReady := False;
  case ls of
    lsFile: Loader := HtmlLoadFile.Create;
    lsWeb: Loader := HtmlLoadWeb.Create;
  end;
  Loader.Load(Source, HtmlFile);
end;

destructor TMyHTMLParser.Destroy;
begin
  if HtmlFile <> nil then
    HtmlFile.Free;
  Loader := nil;
  Saver := nil;
  inherited Destroy;
end;

function CopyBuffer(StartIndex: PChar; Length: integer): string;
var
  S: string;
begin
  SetLength(S, Length);
  StrLCopy(@S[1], StartIndex, Length);
  Result := S;
end;

function GetTagName(Tag: string): string;
var
  P: PChar;
  S: PChar;
begin
  P := PChar(Tag);
  while P^ in ['<', ' ', #9] do
    Inc(P);
  S := P;
  while not (P^ in ['>']) do
    Inc(P);
  if P > S then
    Result := CopyBuffer(S, P - S)
  else
    Result := '';
end;

function GetTagAttribute(Tag, Attribute: string): string;
var
  P: PChar;
  S: PChar;
  C: char;
begin
  Result := '';
  P := PChar(Tag);
  S := StrPos(P, PChar(Attribute));
  if S <> nil then
  begin
    P := S;

    // Skip attribute name
    while not (P^ in ['=', ' ', '>', #0]) do
      Inc(P);

    if (P^ = '=') then
      Inc(P);

    while not (P^ in [' ', '>', #0]) do
    begin

      if (P^ in ['"', '''']) then
      begin
        C := P^;
        Inc(P); { Skip current character }
      end
      else
        C := ' ';

      { thanks to Dmitry [mail@vader.ru] }
      while not (P^ in [C, '>', #0]) do
        Inc(P);

      if (P^ <> '>') then
        Inc(P); { Skip current character, except '>' }
      Break;
    end;

    if P > S then
      Result := CopyBuffer(S, P - S)
    else
      Result := '';
  end;
end;

function InTags(Tag: string; const TagSet: array of string): boolean;
var
  S: string;
begin
  Result := False;
  for S in TagSet do
    if Tag = S then
      exit(True);
end;

initialization
  TParserFactory.RegisterParser(TMyHTMLParser);
end.
