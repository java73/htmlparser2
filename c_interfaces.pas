unit c_interfaces;

{$mode objfpc}{$H+}

interface

uses
  Classes;

type

  cLoadSource = (lsWeb, lsFile);

  { IHtmlLoader }

  IHtmlLoader = interface
    ['{B9EF826B-AADE-48BF-BE19-D1A9846779C5}']
    procedure Load(Source: string; obj: TStringList);
  end;

  { IHtmlSaver }

  IHtmlSaver = interface
    ['{E5AF49F9-4A35-4872-9C55-F32DFC7CBE60}']
    procedure Save(Target: string; obj: TStringList);
  end;

  { IDataContainer }

  IDataContainer = interface
    ['{1BBE3EA5-4DBC-4AE7-9EC4-42048A275190}']
    procedure Add(Key, KeyType, Value: string);
    function GetValue(Key: string): string;
    function TryToGet(Key: string; out KeyType, Value: string): boolean;
    function PutValue(Key, Value: string): boolean;
  end;

  { IAbstractDoc }

  IAbstractDoc = interface(IDataContainer)
    ['{5F6DA09D-5D7E-4FF1-9928-F73FDD67F965}']
    procedure SetValuesFromDB;
    procedure PutBatchValues(const _Keys: array of string; _Value: string);
  end;

  { IHTMLParser }

  IHTMLParser = interface
    ['{A27B1F66-B610-4808-8FD9-3703AAABD3A8}']
    function Ready: boolean;
    function HtmlAsWString: WideString;
    procedure DoParse(_target: string);
    procedure AssignDoc(ADoc: IAbstractDoc);
  end;

  {$REGION ParserFactory}
  TCustomHTMLClass = class of TCustomHTMLParser;

  { TCustomHTMLParser }

  TCustomHTMLParser = class(TInterfacedObject, IHTMLParser)
  public
    function Ready: boolean; virtual; abstract;
    function HtmlAsWString: WideString; virtual; abstract;
    procedure DoParse(_target: string); virtual; abstract;
    procedure AssignDoc(ADoc: IAbstractDoc); virtual; abstract;
    constructor Create(Source: string; ls: cLoadSource); virtual; abstract;
  end;

  { TParserFactory }

  TParserFactory = class
  private
    class var FParserClass: TCustomHTMLClass;
  public
    class function CreateParser(Source: string; ls: cLoadSource): IHTMLParser;
    class procedure RegisterParser(AParserClass: TCustomHTMLClass);
  end;

  {$ENDREGION}

implementation

{$REGION TParserFactory implementation}

class function TParserFactory.CreateParser(Source: string;
  ls: cLoadSource): IHTMLParser;
begin
  Result := FParserClass.Create(Source, ls);
end;

class procedure TParserFactory.RegisterParser(AParserClass: TCustomHTMLClass);
begin
  FParserClass := AParserClass;
end;

{$ENDREGION}

end.
