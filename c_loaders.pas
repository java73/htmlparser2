unit c_loaders;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fphttpclient, c_interfaces;

type

  { HtmlLoadFile }

  HtmlLoadFile = class(TInterfacedObject, IHtmlLoader)
  public
    procedure Load(Source: string; obj: TStringList);
  end;

  { HtmlLoadWeb }

  HtmlLoadWeb = class(TInterfacedObject, IHtmlLoader)
  public
    procedure Load(Source: string; obj: TStringList);
  end;

  { HtmlSaveToFile }

  HtmlSaveToFile = class (TInterfacedObject, IHtmlSaver)
  public
    procedure Save(Target: string; obj: TStringList);
  end;

implementation

{ HtmlSaveToFile }

procedure HtmlSaveToFile.Save(Target: string; obj: TStringList);
begin
  if (obj<>nil) and (obj.Count>0) then
    obj.SaveToFile(Target);
end;

{ HtmlLoadFile }

procedure HtmlLoadFile.Load(Source: string; obj: TStringList);
begin
     if obj<>nil then obj.LoadFromFile(Source);
end;

{ HtmlLoadWeb }

procedure HtmlLoadWeb.Load(Source: string; obj: TStringList);
begin
     if obj<>nil then obj.Text:=TFPCustomHTTPClient.SimpleGet(Source);
end;

end.

