unit c_DOMparser;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, c_interfaces, c_parser,
  SAX_HTML, dom_html, dom;

type

  { TMyDOMParser }

  TMyDOMParser = class(TMyHTMLParser)
  protected
    HTML: THTMLDocument;
  public
    procedure DoParse(_target: string); override;
    procedure AssignDoc(ADoc: IAbstractDoc); override;
    constructor Create(Source: string; ls: cLoadSource); override;
    destructor Destroy; override;
  end;

implementation

{ TMyDOMParser }



procedure TMyDOMParser.DoParse(_target: string);
begin
  inherited DoParse(_target);
end;

procedure TMyDOMParser.AssignDoc(ADoc: IAbstractDoc);
var
  Elems: array of TDOMNodeList;
  i,j: integer;
  AName, AType, AValue: DOMString;
begin
  if ADoc <> nil then
    begin
      Doc := ADoc;
      SetLength(Elems, Length(PTags));
      for i:=Low(Elems) to High(Elems) do begin
        Elems[i] := HTML.GetElementsByTagName(PTags[i]);
        for j:=0 to Elems[i].Count do begin
          AName := TDOMElement(Elems[i].Item[j]).GetAttribute('name');
          AType := TDOMElement(Elems[i].Item[j]).GetAttribute('type');
          if AType = 'checkbox' then
            AType := 'bool'
          else
            AType := 'str';
          AValue := TDOMElement(Elems[i].Item[j]).TextContent;
          Doc.Add(AName,AType,AValue);
        end;
      end;
      fReady := true;
    end;
  Elems := nil;
end;

constructor TMyDOMParser.Create(Source: string; ls: cLoadSource);
begin
  inherited Create(Source, ls);
  ReadHTMLFile(HTML,TStringStream.Create(HtmlFile.Text));
end;

destructor TMyDOMParser.Destroy;
begin
  FreeAndNil(HTML);
  inherited Destroy;
end;

initialization
  //TParserFactory.RegisterParser(TMyDOMParser);
end.

