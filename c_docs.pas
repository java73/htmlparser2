unit c_docs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, c_interfaces, c_datacontainers;

type

  TCustomDocClass = class of TCustomDoc;

  { TCustomDoc }

  TCustomDoc = class(TInterfacedObject, IAbstractDoc)
  protected
    fData: IDataContainer;
  public
    procedure SetValuesFromDB; virtual; abstract;
    procedure PutBatchValues(const _Keys: array of string; _Value: string);
    procedure Add(Key, KeyType, Value: string);
    function GetValue(Key: string): string;
    function TryToGet(Key: string; out KeyType, Value: string): boolean;
    function PutValue(Key, Value: string): boolean;
    constructor Create;
    destructor Destroy; override;
  end;

  { TDoc1 }

  TDoc1 = class(TCustomDoc)
  private
    function GetRandomStrValue: string;
  public
    property RandomStrValue: string read GetRandomStrValue;
    procedure SetValuesFromDB; override;
  end;

  { TDoc2 }

  TDoc2 = class(TCustomDoc)
    procedure SetValuesFromDB; override;
  end;

implementation

{ TCustomDoc }

function TCustomDoc.PutValue(Key, Value: string): boolean;
begin
  Result := fData.PutValue(Key, Value);
end;

function TCustomDoc.GetValue(Key: string): string;
begin
  Result := fData.GetValue(Key);
end;

procedure TCustomDoc.PutBatchValues(const _Keys: array of string; _Value: string);
var
  S: string;
begin
  for S in _Keys do
    PutValue(S, _Value);
end;

procedure TCustomDoc.Add(Key, KeyType, Value: string);
begin
  fData.Add(Key, KeyType, Value);
end;

function TCustomDoc.TryToGet(Key: string; out KeyType, Value: string): boolean;
begin
  Result := fData.TryToGet(Key, KeyType, Value);
end;

constructor TCustomDoc.Create;
begin
  inherited Create;
  fData := TMapContainer.Create;
end;

destructor TCustomDoc.Destroy;
begin
  fData := nil;
  inherited Destroy;
end;

{ TDoc1 }

function TDoc1.GetRandomStrValue: string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to 9 do
    Result += chr(Random(Ord('z') - Ord('a') + 1) + Ord('a'));
end;

procedure TDoc1.SetValuesFromDB;
var
  i: integer;
begin
  for i := 1 to 10 do
    PutValue('pass' + IntToStr(i), RandomStrValue);
end;

{ TDoc2 }

procedure TDoc2.SetValuesFromDB;
begin
  PutValue('input1', 'Поле ввода однострочное');
  PutValue('input2', 'On');
  PutValue('input3', 'Многострочное' + #13#10 + 'поле' +
    #13#10 + 'ввода!');
end;

end.
