unit mainform;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  ActnList, StdCtrls, ExtCtrls, Menus, HtmlView, c_interfaces, c_docs;

const
  TPL_Doc1 = 'templates' + PathDelim + 'doc1.html';
  TPL_Doc2 = 'templates' + PathDelim + 'doc2.html';
//WTPL_LicAgr_Application =
//  'https://bitbucket.org/java73/jhtmlparser/raw/d8c451c392d99a5b98303b122d353067a7398858/11.html';

type

  { TForm1 }

  TForm1 = class(TForm)
    AFill: TAction;
    ASavetofile: TAction;
    ADownload: TAction;
    Actions: TActionList;
    Html: THtmlViewer;
    Memo1: TMemo;
    LoadMenu: TPopupMenu;
    Doc1Menu: TMenuItem;
    Doc2Menu: TMenuItem;
    SaveDlg: TSaveDialog;
    Splitter1: TSplitter;
    ToolBar1: TToolBar;
    LoadBtn: TToolButton;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    procedure ADownloadExecute(Sender: TObject);
    procedure AFillExecute(Sender: TObject);
    procedure ASavetofileExecute(Sender: TObject);
    procedure Doc1MenuClick(Sender: TObject);
    procedure Doc2MenuClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    Parser: IHTMLParser;
    Doc: IAbstractDoc;
    procedure ClearDocAndParser;
    procedure CreateAndShowHtml(_addr: string; _S: cLoadSource = lsFile);
    function ReadFormValue(i: integer): string;
    procedure FillFormValues;
  public
    destructor Destroy; override;
  end;

var
  Form1: TForm1;

implementation

uses HtmlSubs;

{$R *.lfm}

{ TForm1 }

procedure TForm1.ASavetofileExecute(Sender: TObject);
var
  i: integer;
begin
  if (SaveDlg.Execute) and (Parser <> nil) then
  begin
    for i := 0 to Html.FormControlList.Count - 1 do
      Doc.PutValue(Html.FormControlList[i].Name, ReadFormValue(i));
    Parser.DoParse(SaveDlg.FileName);
    Html.LoadFromString(Parser.HtmlAsWString);
  end;
end;

procedure TForm1.Doc1MenuClick(Sender: TObject);
begin
  ClearDocAndParser;
  Doc := TDoc1.Create;
  CreateAndShowHtml(TPL_Doc1);
  Self.Caption := 'Открытый документ: ' + TPL_Doc1;
end;

procedure TForm1.Doc2MenuClick(Sender: TObject);
begin
  ClearDocAndParser;
  Doc := TDoc2.Create;
  CreateAndShowHtml(TPL_Doc2);
  Self.Caption := 'Открытый документ: ' + TPL_Doc2;
end;

procedure TForm1.ADownloadExecute(Sender: TObject);
begin
  //CreateAndShowHtml(WTPL_LicAgr_Application, lsWeb);
end;

procedure TForm1.AFillExecute(Sender: TObject);
begin
  if Doc <> nil then
    Doc.SetValuesFromDB;
  FillFormValues;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  Html.Clear;
  Memo1.Lines.Clear;
end;

procedure TForm1.CreateAndShowHtml(_addr: string; _S: cLoadSource);
begin
  Screen.Cursor := crHourGlass;
  Parser := TParserFactory.CreateParser(_addr, _S);
  Parser.AssignDoc(Doc);
  if Parser.Ready then
    Html.LoadFromString(Parser.HtmlAsWString);
  Screen.Cursor := crDefault;
end;

procedure TForm1.ClearDocAndParser;
begin
  if Parser <> nil then
    Parser := nil;
  if Doc <> nil then
    Doc := nil;
end;

function TForm1.ReadFormValue(i: integer): string;
begin
  Result := '';
  if (TFormControlObj(HTML.FormControlList.Items[i]).TheControl is TCheckbox) and
    (TCheckbox(TFormControlObj(HTML.FormControlList.Items[i]).TheControl).Checked) then
    Result := 'On';
  if (TFormControlObj(HTML.FormControlList.Items[i]).TheControl is TEdit) then
    Result := TEdit(TFormControlObj(HTML.FormControlList.Items[i]).TheControl).Text;
  if (TFormControlObj(HTML.FormControlList.Items[i]).TheControl is TMemo) then
    Result := TMemo(TFormControlObj(HTML.FormControlList.Items[i]).TheControl).Text;
end;

procedure TForm1.FillFormValues;
var
  i: integer;
  ValStr: string;
begin
  with Html.FormControlList do
    for i := 0 to Count - 1 do
    begin
      ValStr := Doc.GetValue(Items[i].Name);
      if TFormControlObj(Items[i]).TheControl is TCheckbox then
      begin
        if ValStr = 'On' then
          TCheckbox(TFormControlObj(Items[i]).TheControl).Checked := True
        else
          TCheckbox(TFormControlObj(Items[i]).TheControl).Checked := False;
      end;
      if (TFormControlObj(Items[i]).TheControl is TEdit) then
        TEdit(TFormControlObj(Items[i]).TheControl).Text := ValStr;
      if (TFormControlObj(Items[i]).TheControl is TMemo) then
        TMemo(TFormControlObj(Items[i]).TheControl).Lines.Text := ValStr;
    end;
end;

destructor TForm1.Destroy;
begin
  Parser := nil;
  Doc := nil;
  inherited Destroy;
end;

end.
