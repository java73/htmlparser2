unit c_datacontainers;

{$mode objfpc}{$H+}
{$MODESWITCH ADVANCEDRECORDS}

interface

uses
  Classes, SysUtils, c_interfaces, gmap, gutil;

type

  { TMapContainer }

  TMapElement = record
    KeyType: string;
    Value: string;
    function Create(_KeyType, _Value: string): TMapElement;
  end;

  less = specialize TLess<string>;
  TTagsMap = specialize TMap<string, TMapElement, less>;

  TMapContainer = class(TInterfacedObject, IDataContainer)
  private
    fData: TTagsMap;
  public
    procedure Add(_Key, _KeyType, _Value: string);
    function GetValue(_Key: string): string;
    function PutValue(_Key, _Value: string): boolean;
    function TryToGet(Key: string; out KeyType, Value: string): boolean;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TMapElement }

function TMapElement.Create(_KeyType, _Value: string): TMapElement;
begin
  Result.KeyType := _KeyType;
  Result.Value := _Value;
end;

{ TMapContainer }

procedure TMapContainer.Add(_Key, _KeyType, _Value: string);
begin
  fData.Insert(_Key, TMapElement.Create(_KeyType, _Value));
end;

function TMapContainer.GetValue(_Key: string): string;
var
  E: TMapElement;
begin
  Result := '';
  if fData.TryGetValue(_Key, E) then
    Result := E.Value;
end;

function TMapContainer.PutValue(_Key, _Value: string): boolean;
var
  E: TMapElement;
begin
  Result := False;
  if fData.TryGetValue(_Key, E) then
  begin
    E.Value := _Value;
    fData[_Key] := E;
    Result := True;
  end;
end;

function TMapContainer.TryToGet(Key: string; out KeyType, Value: string): boolean;
var
  E: TMapElement;
begin
  Result := False;
  if fData.TryGetValue(Key, E) then
  begin
    KeyType := E.KeyType;
    Value := E.Value;
    Result := True;
  end;

end;

constructor TMapContainer.Create;
begin
  fData := TTagsMap.Create;
end;

destructor TMapContainer.Destroy;
begin
  fData.Free;
  inherited Destroy;
end;

end.
